module gitlab.com/microservices-dumb-example/users-service

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.3.3
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/satori/go.uuid v1.2.0
	gitlab.com/microservices-dumb-example/proto-go v1.1.0
	gitlab.com/redpanda-utils/wrapper v1.3.1
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	google.golang.org/grpc v1.28.0
)

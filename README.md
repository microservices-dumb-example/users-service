# Users service
> Users service application

Users service provides information and login utilities for users.

* [Grpc users service](#grpc-users-service)
  * [Add](#add)
  * [Get](#get)
  * [Login](#login)
  * [Atoken](#atoken)
  * [Rtoken](#rtoken)
  * [Validate](#validate)
* [Configuration](#configuration)
* [Docker](#docker)

## Grpc users service

The main service is the
[users grpc service](https://gitlab.com/microservices-dumb-example/proto#users).

### Add

This endpoint adds a new user. The request
[User](https://gitlab.com/microservices-dumb-example/proto#user) is the
following:

| Variable name | Type | Description | Is required? |
|---------------|------|-------------|--------------|
| username | string | New user username | Yes |
| password | string | New user password | Yes |

The response [User](https://gitlab.com/microservices-dumb-example/proto#user) is
the following:

| Variable name | Type | Description |
|---------------|------|-------------|
| user_uuid | string | Added user UUID |
| username | string | Added user username |
| created_at | google.protobuf.Timestamp | Added user creation time |

The following error codes can be returned:

* Already exists
  * Username already in use
* Internal
  * Error generating password hash
  * Error adding new user
* Invalid argument
  * Missing username on request
  * Missing password on request

### Get

This endpoint gets a user. The request
[User](https://gitlab.com/microservices-dumb-example/proto#user) is the
following:

| Variable name | Type | Description | Is required? |
|---------------|------|-------------|--------------|
| user_uuid | string | User UUID | Yes if username is not set |
| username | string | Username | Yes if user_uuid is not set |

The response [User](https://gitlab.com/microservices-dumb-example/proto#user) is
the following:

| Variable name | Type | Description |
|---------------|------|-------------|
| user_uuid | string | Added user UUID |
| username | string | Added user username |
| created_at | google.protobuf.Timestamp | Added user creation time |

The following error codes can be returned:

* Invalid argument
  * Missing user_uuid and username on request
  * Wrongformed user_uuid
* Not found
  * Not found user_uuid
  * Not found username

### Login

This endpoints logins a user. The request
[User](https://gitlab.com/microservices-dumb-example/proto#user) is the
following:

| Variable name | Type | Description | Is required? |
|---------------|------|-------------|--------------|
| username | string | User username | Yes |
| password | string | User password | Yes |

The response [Jwt](https://gitlab.com/microservices-dumb-example/proto#jwt) is
the following:

| Variable name | Type | Description |
|---------------|------|-------------|
| token | string | Signed JWT token |
| exp | google.protobuf.Timestamp | Expiration of the token |
| iat | google.protobuf.Timestamp | Issued time of the token |

The following error codes can be returned:

* Internal
  * Error generating refresh token
  * Error adding refresh token to database
* Invalid argument
  * Missing username on request
  * Missing password on request
* Unauthenticated
  * Unknown username
  * Bad password

### Atoken

This endpoints returns an access token. The request
[Jwt](https://gitlab.com/microservices-dumb-example/proto#jwt) is the following:

| Variable name | Type | Description | Is required? |
|---------------|------|-------------|--------------|
| token | string | Refresh token | Yes |

The response [Jwt](https://gitlab.com/microservices-dumb-example/proto#jwt) is
the following:

| Variable name | Type | Description |
|---------------|------|-------------|
| token | string | Signed access JWT token |
| exp | google.protobuf.Timestamp | Expiration of the token |
| iat | google.protobuf.Timestamp | Issued time of the token |

The following error codes can be returned:

* Internal
  * Error generating access token
* Invalid argument
  * Missing refresh token on request
  * Missing subject on token
  * Wronformed subject on token
* Not found
  * Subject not found
* Unauthenticated
  * Wrong refresh token
  * No refresh tokens for user
  * Unknown refresh token

### Rtoken

This endpoints returns a arefresh token. The request
[Jwt](https://gitlab.com/microservices-dumb-example/proto#jwt) is the following:

| Variable name | Type | Description | Is required? |
|---------------|------|-------------|--------------|
| token | string | Refresh token | Yes |

The response [Jwt](https://gitlab.com/microservices-dumb-example/proto#jwt) is
the following:

| Variable name | Type | Description |
|---------------|------|-------------|
| token | string | Signed refresh JWT token |
| exp | google.protobuf.Timestamp | Expiration of the token |
| iat | google.protobuf.Timestamp | Issued time of the token |

The following error codes can be returned:

* Internal
  * Error generating refresh token
  * Error adding refresh token to database
* Invalid argument
  * Missing refresh token on request
  * Missing subject on token
  * Wronformed subject on token
* Not found
  * Subject not found
* Unauthenticated
  * Wrong refresh token
  * No refresh tokens for user
  * Unknown refresh token

### Validate

This endpoint validates an access token. The request
[Jwt](https://gitlab.com/microservices-dumb-example/proto#jwt) is the following:

| Variable name | Type | Description | Is required? |
|---------------|------|-------------|--------------|
| token | string | Access token | Yes |

The response [User](https://gitlab.com/microservices-dumb-example/proto#user) is
the following:

| Variable name | Type | Description |
|---------------|------|-------------|
| user_uuid | string | Added user UUID |
| username | string | Added user username |
| created_at | google.protobuf.Timestamp | Added user creation time |

The following error codes can be returned:

* Invalid argument
  * Missing access token on request
  * Missing subject on token
  * Wrongformed subject on token
* Not found
  * Subject not found
* Unauthenticated
  * Wrong refresh token

## Configuration

To configure all of the service options you can set the proper environment
variables:

| Variable name | Description | Is required? |
|---------------|-------------|--------------|
| USERS_SERVICE_PORT | Grpc users service serving port | Yes |
| USERS_SERVICE_SECRET | Users service secret | Yes |
| USERS_SERVICE_CERT | Users service SSL certificate | Yes |
| USERS_SERVICE_KEY | Users service SSL key | Yes |
| USERS_SERVICE_INTERFACE | Grpc users service serving interface | No; defaults to 0.0.0.0 |
| USERS_DATABASE_HOST | Users database host | Yes |
| USERS_DATABASE_NAME | Users database name | Yes |
| USERS_DATABASE_USER | Users database user | Yes |
| USERS_DATABASE_PASSWORD | Users database password | No; defaults to an empty string |
| USERS_DATABASE_PORT | Users database serving port | No; defaults to 5432 |
| USERS_DATABASE_SCHEMA | Users database schema | No; defaults to public |

## Docker

To create a docker image run the following command:

```
[user@host] $ docker build -t image_name:tag .
```

Then you can run a new instance of the service:

```
[user@host] $ docker run -d --restart unless-stopped --name=container_name \
  image_name:tag
```

Remember to use the *-e* argument to pass all the environment variables.

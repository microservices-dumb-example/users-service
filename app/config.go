/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package app

/* Config structure */
type Config struct {
	usersSecret           string
	usersGrpcInterface    string
	usersGrpcPort         string
	usersCert             string
	usersKey              string
	usersDatabaseHost     string
	usersDatabasePort     string
	usersDatabaseName     string
	usersDatabaseSchema   string
	usersDatabaseUser     string
	usersDatabasePassword string
}

/* Sets users secret */
func (c *Config) SetUsersSecret(usersSecret string) string {
	c.usersSecret = usersSecret
	return c.usersSecret
}

/* Gets users secret */
func (c *Config) GetUsersSecret() string {
	return c.usersSecret
}

/* Sets users grpc server interface */
func (c *Config) SetUsersGrpcInterface(usersGrpcInterface string) string {
	c.usersGrpcInterface = usersGrpcInterface
	return c.usersGrpcInterface
}

/* Gets users grpc server interface */
func (c *Config) GetUsersGrpcInterface() string {
	return c.usersGrpcInterface
}

/* Sets users grpc server port */
func (c *Config) SetUsersGrpcPort(usersGrpcPort string) string {
	c.usersGrpcPort = usersGrpcPort
	return c.usersGrpcPort
}

/* Gets users grpc server port */
func (c *Config) GetUsersGrpcPort() string {
	return c.usersGrpcPort
}

/* Sets users cert */
func (c *Config) SetUsersCert(usersCert string) string {
	c.usersCert = usersCert
	return c.usersCert
}

/* Gets users cert */
func (c *Config) GetUsersCert() string {
	return c.usersCert
}

/* Sets users key */
func (c *Config) SetUsersKey(usersKey string) string {
	c.usersKey = usersKey
	return c.usersKey
}

/* Gets users key */
func (c *Config) GetUsersKey() string {
	return c.usersKey
}

/* Sets users database connection host */
func (c *Config) SetUsersDatabaseHost(usersDatabaseHost string) string {
	c.usersDatabaseHost = usersDatabaseHost
	return c.usersDatabaseHost
}

/* Gets users database connection host */
func (c *Config) GetUsersDatabaseHost() string {
	return c.usersDatabaseHost
}

/* Sets users database connection port */
func (c *Config) SetUsersDatabasePort(usersDatabasePort string) string {
	c.usersDatabasePort = usersDatabasePort
	return c.usersDatabasePort
}

/* Gets users database connection port */
func (c *Config) GetUsersDatabasePort() string {
	return c.usersDatabasePort
}

/* Sets users database connection database name */
func (c *Config) SetUsersDatabaseName(usersDatabaseName string) string {
	c.usersDatabaseName = usersDatabaseName
	return c.usersDatabaseName
}

/* Gets users database connection database name */
func (c *Config) GetUsersDatabaseName() string {
	return c.usersDatabaseName
}

/* Sets users database connection schema */
func (c *Config) SetUsersDatabaseSchema(usersDatabaseSchema string) string {
	c.usersDatabaseSchema = usersDatabaseSchema
	return c.usersDatabaseSchema
}

/* Gets users database connection schema */
func (c *Config) GetUsersDatabaseSchema() string {
	return c.usersDatabaseSchema
}

/* Sets users database connection user */
func (c *Config) SetUsersDatabaseUser(usersDatabaseUser string) string {
	c.usersDatabaseUser = usersDatabaseUser
	return c.usersDatabaseUser
}

/* Gets users database connection user */
func (c *Config) GetUsersDatabaseUser() string {
	return c.usersDatabaseUser
}

/* Sets users database connection password */
func (c *Config) SetUsersDatabasePassword(usersDatabasePassword string) string {
	c.usersDatabasePassword = usersDatabasePassword
	return c.usersDatabasePassword
}

/* Gets users database connection password */
func (c *Config) GetUsersDatabasePassword() string {
	return c.usersDatabasePassword
}

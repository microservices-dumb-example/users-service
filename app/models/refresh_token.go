/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package models

import (
	"errors"
	"fmt"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/satori/go.uuid"
	"gitlab.com/redpanda-utils/wrapper"
	"golang.org/x/crypto/bcrypt"
)

/* Refresh token validity */
const refreshTokenValidity int64 = 5184000

/* Structure to define a refresh token */
type RefreshToken struct {
	Owner       int       `gorm:"column:owner;primary key"`
	Token       string    `gorm:"column:token;primary key"`
	CreatedAt   time.Time `gorm:"column:created_at;not null"`
	ExpiresAt   time.Time `gorm:"column:created_at;not null"`
	SignedToken string
	OwnerUser   *User
}

/* Creates a new refresh token */
func NewRefreshToken(secret string, user *User) (*RefreshToken, error) {
	refreshToken := new(RefreshToken)

	now := time.Now()

	jwtToken := newToken(now.Unix()+refreshTokenValidity, now.Unix(),
		user.UserUUID.String())

	signedToken, err := jwtToken.SignedString([]byte(secret))
	if err != nil {
		return nil, err
	}

	split := strings.Split(signedToken, ".")
	if len(split) != 3 {
		return nil, errors.New("bad split")
	}
	token, err := bcrypt.GenerateFromPassword([]byte(split[2]), 12)
	if err != nil {
		return nil, err
	}

	refreshToken.Owner = user.ID
	refreshToken.Token = string(token)
	refreshToken.CreatedAt = now
	refreshToken.ExpiresAt = time.Unix(now.Unix()+refreshTokenValidity, 0)
	refreshToken.SignedToken = signedToken

	return refreshToken, nil
}

/* Validates a refresh token */
func ValidateRefreshToken(w *wrapper.Wrapper, token, secret string) (
	*RefreshToken, error) {
	claims := jwt.MapClaims{}
	jwtToken, err := jwt.ParseWithClaims(token, claims,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		})
	if err != nil {
		return nil, err
	}

	userUUIDRaw, ok := claims["sub"].(string)
	if !ok {
		return nil, errors.New("missing subject on token")
	}

	userUUID, err := uuid.FromString(userUUIDRaw)
	if err != nil {
		return nil, err
	}
	user, err := GetUserUserUUID(w, userUUID)
	if err != nil {
		return nil, err
	}

	tokens, err := GetRefreshTokens(w, user)
	if err != nil {
		return nil, err
	}

	found := false
	i := 0
	for i < len(tokens) && !found {
		err := bcrypt.CompareHashAndPassword(
			[]byte(tokens[i].Token),
			[]byte(jwtToken.Signature),
		)
		if err == nil {
			found = true
		} else {
			i++
		}
	}
	if !found {
		return nil, errors.New("not valid token")
	}

	tokens[i].OwnerUser = user

	return &tokens[i], nil
}

/* Retrieves a refresh token from database */
func getRefreshTokenDatabase(conn *gorm.DB, owner int, token string) (
	*RefreshToken, error) {
	refreshToken := new(RefreshToken)

	err := conn.Where("owner = ? AND token = ?", owner, token).First(
		&refreshToken).Error
	if err != nil {
		return nil, err
	}

	return refreshToken, nil
}

/* Retrieves refresh tokens from database */
func getRefreshTokensDatabase(conn *gorm.DB, user *User) ([]RefreshToken,
	error) {
	var refreshTokens []RefreshToken

	err := conn.Where("owner = ?", user.ID).Find(&refreshTokens).Error
	if err != nil {
		return nil, err
	}

	return refreshTokens, nil
}

/* Retrieves a refresh token */
func GetRefreshToken(w *wrapper.Wrapper, owner int, token string) (
	*RefreshToken, error) {
	conn, err := w.PsqlConn()
	if err != nil {
		return nil, err
	}

	return getRefreshTokenDatabase(conn, owner, token)
}

/* Retrieves refresh tokens */
func GetRefreshTokens(w *wrapper.Wrapper, user *User) ([]RefreshToken, error) {
	conn, err := w.PsqlConn()
	if err != nil {
		return nil, err
	}

	return getRefreshTokensDatabase(conn, user)
}

/* Adds a refresh token to database */
func (refreshToken *RefreshToken) addRefreshTokenDatabase(conn *gorm.DB) error {
	insertStatement := `
      INSERT INTO %s (owner, token, created_at, expires_at) VALUES (?, ?, ?, ?)`

	insertStatement = fmt.Sprintf(insertStatement,
		gorm.DefaultTableNameHandler(conn, "refresh_tokens"))

	return conn.Exec(insertStatement, refreshToken.Owner, refreshToken.Token,
		refreshToken.CreatedAt, refreshToken.ExpiresAt).Error
}

/* Adds a new refresh token */
func (refreshToken *RefreshToken) AddRefreshToken(w *wrapper.Wrapper) error {
	conn, err := w.PsqlConn()
	if err != nil {
		return err
	}

	return refreshToken.addRefreshTokenDatabase(conn)
}

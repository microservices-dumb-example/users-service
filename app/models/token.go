/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package models

import (
	"fmt"

	jwt "github.com/dgrijalva/jwt-go"
)

/* Token audience */
const tokenAudience string = "microservices-dumb-example"

/* Token issuer */
const tokenIssuer string = "gitlab.com/dogspeed"

/* Creates a new token */
func newToken(exp, iat int64, sub string) *jwt.Token {
	id := fmt.Sprintf("%s:%d", sub, iat)

	claims := jwt.StandardClaims{
		Audience:  tokenAudience,
		ExpiresAt: exp,
		Id:        id,
		IssuedAt:  iat,
		Issuer:    tokenIssuer,
		NotBefore: iat,
		Subject:   sub,
	}

	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
}

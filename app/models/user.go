/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package models

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/satori/go.uuid"
	"gitlab.com/redpanda-utils/wrapper"
	"golang.org/x/crypto/bcrypt"
)

/* Structure to define a user */
type User struct {
	ID        int       `gorm:"column:id;auto_increment;primary key"`
	UserUUID  uuid.UUID `gorm:"column:user_uuid";unique;not null"`
	Username  string    `gorm:"column:username;unique;not null"`
	Password  string    `gorm:"column:password;not null"`
	CreatedAt time.Time `gorm:"column:created_at;not null"`
}

/* Creates a new user */
func NewUser(username, password string) (*User, error) {
	user := new(User)

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password),
		bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	user.Username = username
	user.Password = string(passwordHash)
	user.CreatedAt = time.Now()

	return user, nil
}

/* Returns a database filter of users by id */
func filterUserIDDatabase(conn *gorm.DB, id int) *gorm.DB {
	return conn.Where("id = ?", id)
}

/* Returns a database filter of users by user_uuid */
func filterUserUserUUIDDatabase(conn *gorm.DB, userUUID uuid.UUID) *gorm.DB {
	return conn.Where("user_uuid = ?", userUUID.String())
}

/* Returns a database filter of users by username */
func filterUserUsernameDatabase(conn *gorm.DB, username string) *gorm.DB {
	return conn.Where("username = ?", username)
}

/* Retrieves a user from database */
func getUserDatabase(filter *gorm.DB) (*User, error) {
	user := new(User)

	if err := filter.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

/* Retrieves a user given an ID */
func GetUserID(w *wrapper.Wrapper, id int) (*User, error) {
	conn, err := w.PsqlConn()
	if err != nil {
		return nil, err
	}

	return getUserDatabase(filterUserIDDatabase(conn, id))
}

/* Retrieves a user given a UserUUID */
func GetUserUserUUID(w *wrapper.Wrapper, userUUID uuid.UUID) (*User, error) {
	conn, err := w.PsqlConn()
	if err != nil {
		return nil, err
	}

	return getUserDatabase(filterUserUserUUIDDatabase(conn, userUUID))
}

/* Retrieves a user given a Username */
func GetUserUsername(w *wrapper.Wrapper, username string) (*User, error) {
	conn, err := w.PsqlConn()
	if err != nil {
		return nil, err
	}

	return getUserDatabase(filterUserUsernameDatabase(conn, username))
}

/* Adds a user to database */
func (user *User) addUserDatabase(conn *gorm.DB) error {
	insertStatement := `
      INSERT INTO %s (username, password, created_at) VALUES (?, ?, ?)`

	insertStatement = fmt.Sprintf(insertStatement,
		gorm.DefaultTableNameHandler(conn, "users"))

	return conn.Exec(insertStatement, user.Username, user.Password,
		user.CreatedAt).Error
}

/* Adds a new user */
func (user *User) AddUser(w *wrapper.Wrapper) error {
	conn, err := w.PsqlConn()
	if err != nil {
		return err
	}

	if err = user.addUserDatabase(conn); err != nil {
		return err
	}

	insertedUser, err := getUserDatabase(filterUserUsernameDatabase(conn,
		user.Username))
	user.UserUUID = insertedUser.UserUUID

	return err
}

/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package models

import (
	"time"
)

/* Refresh token validity */
const accessTokenValidity int64 = 300

/* Structure to define an access token */
type AccessToken struct {
	Owner       *User
	SignedToken string
	CreatedAt   time.Time
	ExpiresAt   time.Time
}

/* Creates a new access token */
func NewAccessToken(secret string, user *User) (*AccessToken, error) {
	accessToken := new(AccessToken)

	now := time.Now()

	jwtToken := newToken(now.Unix()+accessTokenValidity, now.Unix(),
		user.UserUUID.String())

	signedToken, err := jwtToken.SignedString([]byte(secret))
	if err != nil {
		return nil, err
	}

	accessToken.Owner = user
	accessToken.SignedToken = signedToken
	accessToken.CreatedAt = now
	accessToken.ExpiresAt = time.Unix(now.Unix()+refreshTokenValidity, 0)

	return accessToken, nil
}

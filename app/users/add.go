/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	"log"

	"github.com/golang/protobuf/ptypes"
	pb "gitlab.com/microservices-dumb-example/proto-go"
	"gitlab.com/microservices-dumb-example/users-service/app/models"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/* Add user function handler */
func (usersServer *UsersServer) Add(_ context.Context,
	in *pb.User) (*pb.User, error) {
	if in.GetUsername() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument username")
	}

	if in.GetPassword() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument password")
	}

	user, _ := models.GetUserUsername(usersServer.w, in.GetUsername())
	if user != nil {
		return nil, status.Errorf(codes.AlreadyExists, "username already in use")
	}

	var err error
	user, err = models.NewUser(in.GetUsername(), in.GetPassword())
	if err != nil {
		log.Printf("error creating new user: %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error creating new user")
	}

	err = user.AddUser(usersServer.w)
	if err != nil {
		log.Printf("error adding new user: %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error adding new user")
	}

	log.Printf("added new user %s %s", user.UserUUID, user.Username)

	createdAt, err := ptypes.TimestampProto(user.CreatedAt)
	if err != nil {
		createdAt = nil
	}

	return &pb.User{
		UserUuid:  user.UserUUID.String(),
		Username:  user.Username,
		Password:  "",
		CreatedAt: createdAt,
	}, nil
}

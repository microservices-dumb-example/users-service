/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes"
	"github.com/satori/go.uuid"
	pb "gitlab.com/microservices-dumb-example/proto-go"
	"gitlab.com/microservices-dumb-example/users-service/app/models"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/* Validate function handler */
func (usersServer *UsersServer) Validate(_ context.Context,
	in *pb.Jwt) (*pb.User, error) {
	if in.GetToken() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument token")
	}

	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(in.GetToken(), claims,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(usersServer.secret), nil
		})
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "bad credentials")
	}

	userUUIDRaw, ok := claims["sub"].(string)
	if !ok {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing subject on token")
	}

	userUUID, err := uuid.FromString(userUUIDRaw)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	user, err := models.GetUserUserUUID(usersServer.w, userUUID)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, err.Error())
	}

	createdAt, err := ptypes.TimestampProto(user.CreatedAt)
	if err != nil {
		createdAt = nil
	}

	return &pb.User{
		UserUuid:  user.UserUUID.String(),
		Username:  user.Username,
		Password:  "",
		CreatedAt: createdAt,
	}, nil
}

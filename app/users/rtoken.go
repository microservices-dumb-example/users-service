/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	"log"

	"github.com/golang/protobuf/ptypes"
	pb "gitlab.com/microservices-dumb-example/proto-go"
	"gitlab.com/microservices-dumb-example/users-service/app/models"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/* Rtoken function handler */
func (usersServer *UsersServer) Rtoken(_ context.Context,
	in *pb.Jwt) (*pb.Jwt, error) {
	if in.GetToken() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument token")
	}

	refreshToken, err := models.ValidateRefreshToken(usersServer.w,
		in.GetToken(), usersServer.secret)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, err.Error())
	}

	refreshToken, err = models.NewRefreshToken(usersServer.secret,
		refreshToken.OwnerUser)
	if err != nil {
		log.Printf("error creating refresh token: %s", err.Error())
		return nil, status.Errorf(codes.Internal,
			"error generating refresh token")
	}

	err = refreshToken.AddRefreshToken(usersServer.w)
	if err != nil {
		log.Printf("error adding new refresh token: %s", err.Error())
		return nil, status.Errorf(codes.Internal,
			"error adding new refresh token")
	}

	expiresAt, err := ptypes.TimestampProto(refreshToken.ExpiresAt)
	if err != nil {
		expiresAt = nil
	}

	createdAt, err := ptypes.TimestampProto(refreshToken.CreatedAt)
	if err != nil {
		createdAt = nil
	}

	return &pb.Jwt{
		Token: refreshToken.SignedToken,
		Exp:   expiresAt,
		Iat:   createdAt,
	}, nil
}

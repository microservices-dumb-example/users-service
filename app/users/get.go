/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	"github.com/golang/protobuf/ptypes"
	"github.com/satori/go.uuid"
	pb "gitlab.com/microservices-dumb-example/proto-go"
	"gitlab.com/microservices-dumb-example/users-service/app/models"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/* Get user function handler */
func (usersServer *UsersServer) Get(_ context.Context,
	in *pb.User) (*pb.User, error) {
	if in.GetUserUuid() == "" && in.GetUsername() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing argument user_uuid or username")
	}

	var user *models.User
	var err error
	if in.GetUserUuid() != "" {
		userUUID, err := uuid.FromString(in.GetUserUuid())
		if err != nil {
			return nil, status.Errorf(codes.InvalidArgument, err.Error())
		}
		user, err = models.GetUserUserUUID(usersServer.w, userUUID)
		if err != nil {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
	} else {
		user, err = models.GetUserUsername(usersServer.w, in.GetUsername())
		if err != nil {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
	}

	createdAt, err := ptypes.TimestampProto(user.CreatedAt)
	if err != nil {
		createdAt = nil
	}

	return &pb.User{
		UserUuid:  user.UserUUID.String(),
		Username:  user.Username,
		Password:  "",
		CreatedAt: createdAt,
	}, nil
}

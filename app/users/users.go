/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	"log"
	"sync"

	pb "gitlab.com/microservices-dumb-example/proto-go"
	"gitlab.com/redpanda-utils/wrapper"
)

/* Server structure */
type UsersServer struct {
	w      *wrapper.Wrapper
	secret string
	pb.UnimplementedUsersServer
}

/* Creates a new users server */
func NewUsersServer(grpcInterface, grpcPort, databaseHost, databasePort,
	databaseName, databaseSchema, databaseUser,
	databasePassword, secret, cert, key string) (*UsersServer, error) {
	usersServer := new(UsersServer)

	usersServer.w = wrapper.NewWrapper()

	usersServer.w.OpenGrpcTLSServer(grpcInterface, grpcPort, cert, key)
	grpcServer, err := usersServer.w.GrpcServer()
	if err != nil {
		return nil, err
	}

	usersServer.w.OpenPsqlConn(databaseHost, databasePort, databaseName,
		databaseSchema, databaseUser, databasePassword)
	if _, err := usersServer.w.PsqlConn(); err != nil {
		log.Printf("error openning database connection: %s", err.Error())
	}

	pb.RegisterUsersServer(grpcServer, usersServer)

	usersServer.secret = secret

	return usersServer, nil
}

/* Serves users server */
func (usersServer *UsersServer) Serve(wg *sync.WaitGroup) {
	defer wg.Done()

	log.Printf("starting users grpc server")

	usersServer.w.ServeGrpcServer()
}

/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package app

import (
	"sync"

	"gitlab.com/microservices-dumb-example/users-service/app/users"
)

/* Application structure */
type App struct {
	usersServer *users.UsersServer
}

/* Creates a new application */
func NewApp(c *Config) (*App, error) {
	app := new(App)

	var err error
	app.usersServer, err = users.NewUsersServer(c.GetUsersGrpcInterface(),
		c.GetUsersGrpcPort(), c.GetUsersDatabaseHost(), c.GetUsersDatabasePort(),
		c.GetUsersDatabaseName(), c.GetUsersDatabaseSchema(),
		c.GetUsersDatabaseUser(), c.GetUsersDatabasePassword(),
		c.GetUsersSecret(), c.GetUsersCert(), c.GetUsersKey())
	if err != nil {
		return nil, err
	}

	return app, nil
}

/* Serves application */
func (app *App) Serve() {
	var wg sync.WaitGroup

	wg.Add(1)
	go app.usersServer.Serve(&wg)

	/* Wait for all routines to end */
	wg.Wait()
}

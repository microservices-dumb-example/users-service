/*
   users service
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/microservices-dumb-example/users-service/app"
)

/* Required environment variables */
var requiredEnvVars = [...]string{
	"USERS_SERVICE_PORT",
	"USERS_SERVICE_SECRET",
	"USERS_SERVICE_CERT",
	"USERS_SERVICE_KEY",
	"USERS_DATABASE_HOST",
	"USERS_DATABASE_NAME",
	"USERS_DATABASE_USER",
}

/* Default users service serving interface */
const defaultUsersServiceInterface string = "0.0.0.0"

/* Default users database password */
const defaultUsersDatabasePassword string = ""

/* Default users database port */
const defaultUsersDatabasePort string = "5432"

/* Default users database schema */
const defaultUsersDatabaseSchema string = "develop"

func main() {
	/* Create application config */
	appConfig := new(app.Config)

	/* Load configuration */
	godotenv.Load()

	for _, envVar := range requiredEnvVars {
		if _, exists := os.LookupEnv(envVar); !exists {
			log.Printf("error missing required environment variable %s", envVar)
			os.Exit(1)
		}
	}

	appConfig.SetUsersGrpcPort(os.Getenv("USERS_SERVICE_PORT"))
	appConfig.SetUsersSecret(os.Getenv("USERS_SERVICE_SECRET"))
	appConfig.SetUsersCert(os.Getenv("USERS_SERVICE_CERT"))
	appConfig.SetUsersKey(os.Getenv("USERS_SERVICE_KEY"))
	appConfig.SetUsersDatabaseHost(os.Getenv("USERS_DATABASE_HOST"))
	appConfig.SetUsersDatabaseName(os.Getenv("USERS_DATABASE_NAME"))
	appConfig.SetUsersDatabaseUser(os.Getenv("USERS_DATABASE_USER"))

	usersServiceInterface, exists := os.LookupEnv("USERS_SERVICE_INTERFACE")
	if !exists {
		usersServiceInterface = defaultUsersServiceInterface
		log.Printf("using default users service interface: %s",
			defaultUsersServiceInterface)
	}
	appConfig.SetUsersGrpcInterface(usersServiceInterface)
	usersDatabasePassword, exists := os.LookupEnv("USERS_DATABASE_PASSWORD")
	if !exists {
		usersDatabasePassword = defaultUsersDatabasePassword
		log.Printf("using default users database password: %s",
			defaultUsersDatabasePassword)
	}
	appConfig.SetUsersDatabasePassword(usersDatabasePassword)
	usersDatabasePort, exists := os.LookupEnv("USERS_DATABASE_PORT")
	if !exists {
		usersDatabasePort = defaultUsersDatabasePort
		log.Printf("using default users database port: %s",
			defaultUsersDatabasePort)
	}
	appConfig.SetUsersDatabasePort(usersDatabasePort)
	usersDatabaseSchema, exists := os.LookupEnv("USERS_DATABASE_SCHEMA")
	if !exists {
		usersDatabaseSchema = defaultUsersDatabaseSchema
		log.Printf("using default users database schema: %s",
			defaultUsersDatabaseSchema)
	}
	appConfig.SetUsersDatabaseSchema(usersDatabaseSchema)

	/* Create application */
	service, err := app.NewApp(appConfig)
	if err != nil {
		log.Printf("error creating application: %s", err.Error())
		os.Exit(1)
	}

	/* Serve application */
	log.Printf("starting service")
	service.Serve()
}
